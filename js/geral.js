
$(function(){
	$(".navbar-collapse ul").addClass('nav navbar-nav');
	$("#collapse > div").removeClass('nav navbar-nav');
	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {

		$('a.scrollTop').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		//CARROSSEL DE DESTAQUE
		$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			animateOut: 'fadeOut',
		});
		var carrosselFotosGaleria = $("#carrosselFotosGaleria").data('owlCarousel');
		$('#carrosselFotosGaleriaLeft').click(function(){ carrosselFotosGaleria.prev(); });
		$('#carrosselFotosGaleriaRight').click(function(){ carrosselFotosGaleria.next(); });	
	});
	var testeMostrarNum = false;
	$(window).scroll( function(){

        $('.positionLeft').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeInLeft animated" );
            }
        });

        $('.positionRight').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeInRight animated" );
            }
        });

         $('.centro').each( function(i){
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_object ){
            	$(this).css('opacity', '1');
                $(this).addClass( "fadeIn animated" );
            }
        });

    });
});
